﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RentService.Service.Model
{
    public class CartItemModel
    {
        public int Id { get; set; }
        public string PreviewImage { get; set; }
        public decimal Price { get; set; }
        public string Name { get; set; }
        public int Quality { get; set; }
        public string Description { get; set; }
    }
}
