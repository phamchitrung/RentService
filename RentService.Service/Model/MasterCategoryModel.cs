﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RentService.Service.Model.Base;

namespace RentService.Service.Model
{
    public class MasterCategoryModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public List<SystemLookupModel> Categories { get; set; } 
    }
}
