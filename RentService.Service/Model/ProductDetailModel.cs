﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RentService.Service.Model.Base;

namespace RentService.Service.Model
{
    public class ProductDetailModel : BaseModel
    {
        public List<string> Images { get; set; }
        public string ProductName { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public string Status { get; set; }
        public int StatusId { get; set; }
    }
}
