﻿namespace RentService.Service.Model.Base
{
    public class ValidationModel
    {
        public string Field { get; set; }
        public string ValidateMessage { get; set; }

        public ValidationModel(string field, string validateMessage)
        {
            Field = field;
            ValidateMessage = validateMessage;
        }
    }
}