﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RentService.Core.Enum;

namespace RentService.Service.Model.Base
{
    public class PagingQueryModel
    {
        public int PageIndex { get; set; }
        public int PageLength { get; set; }
        public Effective Effective { get; set; }

        public PagingQueryModel(int pageIndex, int pageLength, Effective effective = Effective.Current)
        {
            PageIndex = pageIndex;
            PageLength = pageLength;
            Effective = effective;
        }
    }
}
