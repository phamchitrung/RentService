﻿namespace RentService.Service.Model.Base
{
    public class SystemLookupModel
    {
        public int Id { get; set; }
        public string Label { get; set; }
    }
}
