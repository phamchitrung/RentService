﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RentService.Service.Model.Base;
using System.ComponentModel.DataAnnotations;

namespace RentService.Service.Model
{
    public class UserDetailModel : BaseModel
    {
        public string Fullname { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
    }
}
