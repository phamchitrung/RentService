﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RentService.Core.Contract;
using RentService.Core.Entity;
using RentService.Service.Contract;
using RentService.Service.Enum;
using RentService.Service.Model.Base;

namespace RentService.Service.Service
{
    public abstract class ServiceBase : IServiceBase
    {
        private IDao<SystemLookup> lookupDao;
        protected ServiceBase(IDao<SystemLookup> lookupDao)
        {
            this.lookupDao = lookupDao;
        }

        public List<SystemLookupModel> GetSystemLookupByTypeId(int lookupTypeId)
        {
            return this.lookupDao.Query(q => q.SystemLookupTypeId == lookupTypeId).Select(q => new SystemLookupModel()
            {
                Id = q.LookupReference,
                Label = q.Description
            }).ToList();
        }

        public string GetSystemLookupByTypeAndReference(int lookupTypeId, int lookupReferenceId)
        {
            return
                this.lookupDao.Query(q => q.SystemLookupTypeId == lookupTypeId && q.LookupReference == lookupReferenceId)
                    .Select(x => x.Description)
                    .FirstOrDefault();
        }
    }
}
