﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using AutoMapper;
using RentService.Core.Contract;
using RentService.Core.Entity;
using RentService.Service.Contract;
using RentService.Service.Model;
using RentService.Service.Model.Base;

namespace RentService.Service.Service
{
    public class MyAuthenticationService : ServiceBase, IMyAuthenticationService
    {
        private IDao<User> userDao;
        private IDao<SystemLookup> systemLookupDao;

        public MyAuthenticationService(IDao<User> userDao, IDao<SystemLookup> systemLookupDao) : base(systemLookupDao)
        {
            this.userDao = userDao;
            this.systemLookupDao = systemLookupDao;
        }

        public List<ValidationModel> ValidateUser(UserDetailModel account)
        {
            var validations = new List<ValidationModel>();
            if (userDao.Query(q => q.Email == account.Email).Any())
            {
                validations.Add(new ValidationModel("Email", "This email is exist!"));
            }
            return validations;
        }

        public List<ValidationModel> Login(UserDetailModel account)
        {
            var validations = new List<ValidationModel>();
            var user = userDao.Query(q => q.Email == account.Email).FirstOrDefault();
            if (user == null || EncodePass(account.Password) != user.Password)
            {
                validations.Add(new ValidationModel("Password", "Your email or password is incorrect"));
            }
            return validations;
        }

        public void SaveUser(UserDetailModel accountDetail)
        {
            var newUser = Mapper.Map<User>(accountDetail);
            newUser.Password = accountDetail.Id > 0
                ? userDao.Query(q => q.Id == accountDetail.Id).Select(q => q.Password).FirstOrDefault()
                : this.EncodePass(accountDetail.Password);
            userDao.Save(newUser);
        }

        private string EncodePass(string text)
        {
            MD5 md5 = new MD5CryptoServiceProvider();

            //compute hash from the bytes of text
            md5.ComputeHash(Encoding.ASCII.GetBytes(text));

            //get hash result after compute it
            var result = md5.Hash;

            var strBuilder = new StringBuilder();
            for (int i = 0; i < result.Length; i++)
            {
                //change it into 2 hexadecimal digits
                //for each byte
                strBuilder.Append(result[i].ToString("x2"));
            }

            return strBuilder.ToString();
        }

        public UserDetailModel GetUserDetail(string email)
        {
            return Mapper.Map<UserDetailModel>(userDao.Query(q => q.Email == email).FirstOrDefault());
        }

        public int GetUserId(string email)
        {
            return userDao.Query(q => q.Email == email).Select(q => q.Id).FirstOrDefault();
        }

        public void UpdateUser(UserDetailModel userDetail)
        {
            var user = userDao.Query(q => q.Id == userDetail.Id).FirstOrDefault();
            userDetail.Email = user.Email;
            userDetail.Password = user.Password;
            userDao.Save(Mapper.Map<User>(userDetail));
        }
    }
}
