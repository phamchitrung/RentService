﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RentService.Core.Contract;
using RentService.Core.Entity;
using RentService.Service.Contract;
using RentService.Service.Model;

namespace RentService.Service.Service
{
    public class OrderService : ServiceBase, IOrderService
    {
        private IDao<SystemLookup> lookupDao;
        private IDao<Order> orderDao;
        private IDao<OrderProduct> orderProductDao;
        public OrderService(IDao<SystemLookup> lookupDao, IDao<Order> orderDao, IDao<OrderProduct> orderProductDao) : base(lookupDao)
        {
            this.lookupDao = lookupDao;
            this.orderDao = orderDao;
            this.orderProductDao = orderProductDao;
        }

        public void SaveOrder(OrderInformationModel information, List<CartItemModel> products, int userId)
        {
            var order = new Order()
            {
                Address = information.Address,
                CreatedDate = DateTime.Now,
                Note = information.Note,
                Phone = information.Phone,
                StatusId = 1,
                UserId = userId
            };
            var savedOrder = orderDao.Save(order);
            var orderProduct = new List<OrderProduct>();
            products.ForEach(
                x =>
                    orderProduct.Add(new OrderProduct()
                    {
                        OrderId = savedOrder.Id,
                        PricePerEach = x.Price,
                        ProductId = x.Id,
                        Quality = x.Quality
                    }));
            orderProductDao.SaveAll(orderProduct);
        }
    }
}
