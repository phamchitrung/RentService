﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using RentService.Core.Contract;
using RentService.Core.Entity;
using RentService.Core.Entity.BaseEntity;
using RentService.Service.Contract;
using RentService.Service.Enum;
using RentService.Service.Model;
using RentService.Service.Model.Base;

namespace RentService.Service.Service
{
    public class ProductService : ServiceBase, IProductService
    {
        private const int LASTEST_PRODUCT_NUMBER = 6;
        private const int RECOMMENDED_PRODUCT_NUMBER = 6;

        private IDao<SystemLookup> lookupDao;
        private IDao<Product> productDao;
        private IDao<SystemImage> systemImageDao;
        private IDao<ProductImage> productImageDao;

        public ProductService(IDao<SystemLookup> lookupDao, IDao<Product> productDao, IDao<SystemImage> systemImageDao, IDao<ProductImage> productImageDao) : base(lookupDao)
        {
            this.lookupDao = lookupDao;
            this.productDao = productDao;
            this.systemImageDao = systemImageDao;
            this.productImageDao = productImageDao;
        }

        public ProductDetailModel GetProductDetail(int productId)
        {
            var product = productDao.GetEntityById(productId);
            return new ProductDetailModel()
            {
                Id = product.Id,
                Description = product.Description,
                Price = product.Price,
                ProductName = product.ProductName,
                Timestamp = product.Timestamp,
                Images = (from productImage in productImageDao.Query(q => q.ProductId == productId)
                          join systemImage in systemImageDao.Query(q => true) on productImage.ImageId equals systemImage.Id
                          select systemImage.Url).ToList(),
                Status = GetSystemLookupByTypeAndReference((int)SystemLookupTypes.ProductStatus, product.StatusId),
                StatusId = product.StatusId
            };
        }

        public List<CartItemModel> SearchProduct(int? categoryId, string textSearch, PagingQueryModel pageQuery)
        {
            var products = productDao.Query(q => true);
            if (categoryId.HasValue)
            {
                products = products.Where(x => x.CategoryId == categoryId);
            }
            if (!string.IsNullOrEmpty(textSearch))
            {
                products = products.Where(x => x.ProductName.Contains(textSearch));
            }
            var productIds = products.OrderBy(q => q.Id).Skip((pageQuery.PageIndex - 1) * pageQuery.PageLength).Take(pageQuery.PageLength).Select(x => x.Id).ToList();
            return GetProducts(productIds);

        }
        public List<CartItemModel> GetProducts(Dictionary<int, int> cartItems)
        {
            var keys = cartItems.Keys.ToList();
            return productDao.Query(q => keys.Contains(q.Id)).ToList().Select(x => new CartItemModel()
            {
                Id = x.Id,
                Name = x.ProductName,
                Price = x.Price,
                Quality = cartItems[x.Id],
                Description = x.Description,
                PreviewImage = (from productImage in productImageDao.Query(q => true)
                                where productImage.ProductId == x.Id && productImage.IsMain
                                join systemImage in systemImageDao.Query(q => true)
                                    on productImage.ImageId equals systemImage.Id
                                select systemImage.Url).FirstOrDefault()
            }).ToList();
        }

        private List<CartItemModel> GetProducts(List<int> productIds)
        {
            var products = productDao.Query(q => productIds.Contains(q.Id)).Select(x => new CartItemModel()
            {
                Id = x.Id,
                Name = x.ProductName,
                Price = x.Price
            }).ToList();
            foreach (var product in products)
            {
                product.PreviewImage = (from productImage in productImageDao.Query(q => true)
                                        where productImage.ProductId == product.Id && productImage.IsMain
                                        join systemImage in systemImageDao.Query(q => true)
                                            on productImage.ImageId equals systemImage.Id
                                        select systemImage.Url).FirstOrDefault();
            }
            return products;
        }

        public List<CartItemModel> GetLastestProducts()
        {
            var products = productDao.Query(q => true).Select(x => new CartItemModel()
            {
                Id = x.Id,
                Name = x.ProductName,
                Price = x.Price
            }).OrderByDescending(q => q.Id).Take(LASTEST_PRODUCT_NUMBER).ToList();
            foreach (var product in products)
            {
                product.PreviewImage = (from productImage in productImageDao.Query(q => true)
                                        where productImage.ProductId == product.Id && productImage.IsMain
                                        join systemImage in systemImageDao.Query(q => true)
                                            on productImage.ImageId equals systemImage.Id
                                        select systemImage.Url).FirstOrDefault();
            }
            return products;
        }

        public List<CartItemModel> GetRecommendedProducts()
        {
            var products = productDao.Query(q => true).Select(x => new CartItemModel()
            {
                Id = x.Id,
                Name = x.ProductName,
                Price = x.Price
            }).OrderByDescending(q => q.Price).Take(RECOMMENDED_PRODUCT_NUMBER).ToList();
            foreach (var product in products)
            {
                product.PreviewImage = (from productImage in productImageDao.Query(q => true)
                                        where productImage.ProductId == product.Id && productImage.IsMain
                                        join systemImage in systemImageDao.Query(q => true)
                                            on productImage.ImageId equals systemImage.Id
                                        select systemImage.Url).FirstOrDefault();
            }
            return products;
        }
    }
}
