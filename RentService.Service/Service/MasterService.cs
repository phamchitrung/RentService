﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RentService.Core.Contract;
using RentService.Core.Entity;
using RentService.Service.Contract;
using RentService.Service.Enum;
using RentService.Service.Model;
using RentService.Service.Model.Base;

namespace RentService.Service.Service
{
    public class MasterService : ServiceBase, IMasterService
    {
        private IDao<SystemLookup> lookupDao;
        private IDao<Category> categoryDao;

        public MasterService(IDao<SystemLookup> lookupDao, IDao<Category> categoryDao) : base(lookupDao)
        {
            this.lookupDao = lookupDao;
            this.categoryDao = categoryDao;
        }

        public List<MasterCategoryModel> GetCategories()
        {
            var masterCategories = categoryDao.Query(q => !q.MasterCategoryId.HasValue).ToList();
            var results = new List<MasterCategoryModel>();
            foreach (var masterCate in masterCategories)
            {
                results.Add(new MasterCategoryModel()
                {
                    Id = masterCate.Id,
                    Title = masterCate.CategoryName,
                    Categories = categoryDao.Query(q => q.MasterCategoryId == masterCate.Id).Select(x => new SystemLookupModel()
                    {
                        Id = x.Id,
                        Label = x.CategoryName
                    }).ToList()
                });
            }
            return results;
        }
    }
}
