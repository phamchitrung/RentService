﻿using AutoMapper;
using RentService.Core.Entity;
using RentService.Service.Model;

namespace RentService.Service.Profiles
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<UserDetailModel, User>();
            CreateMap<User, UserDetailModel>();
        }
    }
}
