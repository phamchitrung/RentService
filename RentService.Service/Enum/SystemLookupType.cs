﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RentService.Service.Enum
{
    public enum SystemLookupTypes
    {
        ProductStatus = 1,
        OrderStatus = 2,
        SecurityLevel = 3,
        SystemRole = 4,
        TimePeriod = 5
    }

    public enum ProductStatus
    {
        Available = 1,
        OutOfStock = 2,
        Comming = 3
    }
}
