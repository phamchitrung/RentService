﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RentService.Core.Enum
{
    public enum Effective
    {
        All = 0,
        Expired = 1,
        Current = 2,
        Future = 3,
        ExpiredCurrent = 4,
        CurrentFuture = 5,
        ExpiredFuture = 6
    }
}
