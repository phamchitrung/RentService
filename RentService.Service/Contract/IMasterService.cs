﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RentService.Service.Model;
using RentService.Service.Model.Base;

namespace RentService.Service.Contract
{
    public interface IMasterService : IServiceBase
    {
        List<MasterCategoryModel> GetCategories();
    }
}
