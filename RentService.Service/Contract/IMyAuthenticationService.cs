﻿using System.Collections.Generic;
using RentService.Service.Model;
using RentService.Service.Model.Base;

namespace RentService.Service.Contract
{
    public interface IMyAuthenticationService : IServiceBase
    {
        List<ValidationModel> ValidateUser(UserDetailModel account);
        List<ValidationModel> Login(UserDetailModel account);
        void SaveUser(UserDetailModel accountDetail);
        UserDetailModel GetUserDetail(string email);
        int GetUserId(string email);
        void UpdateUser(UserDetailModel userDetail);
    }
}
