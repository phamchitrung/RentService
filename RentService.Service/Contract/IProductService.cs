﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RentService.Core.Entity.BaseEntity;
using RentService.Service.Model;
using RentService.Service.Model.Base;

namespace RentService.Service.Contract
{
    public interface IProductService : IServiceBase
    {
        ProductDetailModel GetProductDetail(int productId);
        List<CartItemModel> SearchProduct(int? categoryId, string textSearch, PagingQueryModel pageQuery);
        List<CartItemModel> GetProducts(Dictionary<int, int> cartItems);
        List<CartItemModel> GetLastestProducts();
        List<CartItemModel> GetRecommendedProducts();
    }
}
