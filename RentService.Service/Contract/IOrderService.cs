﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RentService.Service.Model;

namespace RentService.Service.Contract
{
    public interface IOrderService : IServiceBase
    {
        void SaveOrder(OrderInformationModel information, List<CartItemModel> products, int userId);
    }
}
