﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RentService.Service.Model.Base;

namespace RentService.Service.Contract
{
    public interface IServiceBase
    {
        List<SystemLookupModel> GetSystemLookupByTypeId(int lookupTypeId);
        string GetSystemLookupByTypeAndReference(int lookupTypeId, int lookupReferenceId);
    }
}
