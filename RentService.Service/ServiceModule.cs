﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using AutoMapper;
using RentService.Core;
using RentService.Core.Contract;
using RentService.Core.DAO;
using RentService.Core.Entity;
using RentService.Core.Entity.BaseEntity;
using RentService.Service.Model;
using RentService.Service.Model.Base;
using RentService.Service.Profiles;

namespace RentService.Service
{
    public class ServiceModule : Module
    {
        private string connectionString;
        public ServiceModule(string connString)
        {
            connectionString = connString;
        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterGeneric(typeof(BaseDao<>)).As(typeof(IDao<>));
            builder.RegisterModule(new DataModule(connectionString));
            base.Load(builder);
        }
    }
}
