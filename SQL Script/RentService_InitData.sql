INSERT INTO SystemLookupType (SystemLookupTypeId, Description)
VALUES
(
	1,
	'Product Status'
)

INSERT INTO SystemLookupType (SystemLookupTypeId, Description)
VALUES
(
	2,
	'Order Status'
)

INSERT INTO SystemLookupType (SystemLookupTypeId, Description)
VALUES
(
	3,
	'Security Level'
)

INSERT INTO SystemLookupType (SystemLookupTypeId, Description)
VALUES
(
	4,
	'System Role'
)

INSERT INTO SystemLookupType (SystemLookupTypeId, Description)
VALUES
(
	5,
	'Time Period'
)

INSERT INTO Category(Name, MasterCategoryId)
VALUES ('Master 1', NULL)

INSERT INTO Category(Name, MasterCategoryId)
VALUES ('Master 2', NULL)

INSERT INTO Category(Name, MasterCategoryId)
VALUES ('Master 3', NULL)

INSERT INTO Category(Name, MasterCategoryId)
VALUES ('Cate 1', 1)

INSERT INTO Category(Name, MasterCategoryId)
VALUES ('Cate 2', 2)

INSERT INTO Category(Name, MasterCategoryId)
VALUES ('Cate 3', 3)

INSERT INTO Product(ProductName, Description, Price, Quality, StatusId, CategoryId)
VALUES ('Asus X441SA-WX021D', 'Description 1', 50, 100, 1, 4)

INSERT INTO Product(ProductName, Description, Price, Quality, StatusId, CategoryId)
VALUES ('Iphone 6S', 'Description 2', 50, 100, 1, 5)

INSERT INTO Product(ProductName, Description, Price, Quality, StatusId, CategoryId)
VALUES ('Samsung Galaxy Tab 6', 'Description 3', 50, 100, 1, 6)

INSERT INTO Product(ProductName, Description, Price, Quality, StatusId, CategoryId)
VALUES ('Asus TP201SA-FV0007T', 'Description 4', 50, 100, 1, 4)

INSERT INTO Product(ProductName, Description, Price, Quality, StatusId, CategoryId)
VALUES ('Iphone 7', 'Description 5', 50, 100, 1, 5)

INSERT INTO Product(ProductName, Description, Price, Quality, StatusId, CategoryId)
VALUES ('Samsung Galaxy Tab 6 10inch', 'Description 6', 50, 100, 1, 6)

INSERT INTO dbo.[User]
(
    FullName,
    Email,
    Phone,
    Address,
    Password
)
VALUES
(
    'User 1', -- FullName - varchar
    'email@a.com', -- Email - varchar
    '123', -- Phone - varchar
    'New York', -- Address - varchar
    'efe6398127928f1b2e9ef3207fb82663' -- Password - varchar
)

INSERT INTO dbo.SystemImage
(
    --ImageId - this column value is auto-generated
    Name,
    Url
)
VALUES
(
    -- ImageId - int
    'Iphone 6S', -- Name - varchar
    'http://image.didongviet.vn/wp-content/uploads/2015/09/iphone-6s-plus-rosegold-380.png'
)

INSERT INTO dbo.SystemImage
(
    --ImageId - this column value is auto-generated
    Name,
    Url
)
VALUES
(
    -- ImageId - int
    'Iphone 7', -- Name - varchar
    'http://image.didongviet.vn/wp-content/uploads/2016/09/ip7-lon.png'
)

INSERT INTO dbo.SystemImage
(
    --ImageId - this column value is auto-generated
    Name,
    Url
)
VALUES
(
    -- ImageId - int
    'Asus X441SA-WX021D', -- Name - varchar
    'http://phongvu.vn/gallery/avatar_upload/products/avatar/740_x441sa-wx0211.jpg'
)

INSERT INTO dbo.SystemImage
(
    --ImageId - this column value is auto-generated
    Name,
    Url
)
VALUES
(
    -- ImageId - int
    'Asus TP201SA-FV0007T', -- Name - varchar
    'http://phongvu.vn/gallery/avatar_upload/products/avatar/15580_vivobook-flipP.png'
)

INSERT INTO dbo.SystemImage
(
    --ImageId - this column value is auto-generated
    Name,
    Url
)
VALUES
(
    -- ImageId - int
    'Samsung Galaxy Tab A6', -- Name - varchar
    'http://image.didongviet.vn/wp-content/uploads/2016/05/Untitled-2-demo.png'
)

INSERT INTO dbo.SystemImage
(
    --ImageId - this column value is auto-generated
    Name,
    Url
)
VALUES
(
    -- ImageId - int
    'Samsung Galaxy Tab A6 10inch', -- Name - varchar
    'http://image.didongviet.vn/wp-content/uploads/2016/07/samsung-galaxy-tab-a-101-2016-4003-demo.png'
)

INSERT INTO dbo.ProductImage(ProductId, ImageId, IsMain)
VALUES (7, 3, 1)

INSERT INTO dbo.ProductImage(ProductId, ImageId, IsMain)
VALUES (10, 4, 1)

INSERT INTO dbo.ProductImage(ProductId, ImageId, IsMain)
VALUES (8, 1, 1)

INSERT INTO dbo.ProductImage(ProductId, ImageId, IsMain)
VALUES (11, 2, 1)

INSERT INTO dbo.ProductImage(ProductId, ImageId, IsMain)
VALUES (9, 5, 1)

INSERT INTO dbo.ProductImage(ProductId, ImageId, IsMain)
VALUES (12, 6, 1)

INSERT INTO SystemLookup(SystemLookupId, LookupTypeId, Description, LookupReference)
VALUES(1, 1, 'Available', 1)

INSERT INTO SystemLookup(SystemLookupId, LookupTypeId, Description, LookupReference)
VALUES(2, 1, 'Out of stock', 2)

INSERT INTO SystemLookup(SystemLookupId, LookupTypeId, Description, LookupReference)
VALUES(3, 1, 'Comming', 3)