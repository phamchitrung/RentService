CREATE TABLE dbo.[User](
	UserId int IDENTITY(1,1) NOT NULL PRIMARY KEY,
	FullName varchar(30) NOT NULL,
	Email varchar(100) NOT NULL,
	Phone varchar(20) NULL,
	Address varchar(100) NULL,
	Password varchar(100) NOT NULL,
	Timestamp timestamp not null
)


CREATE TABLE Category(
	CategoryId int IDENTITY(1,1) NOT NULL PRIMARY KEY,
	Name varchar(30) NOT NULL,
	MasterCategoryId int NULL,
	Timestamp timestamp not null
)

CREATE TABLE dbo.Product(
	ProductId int IDENTITY(1,1) NOT NULL PRIMARY KEY,
	ProductName varchar(30) NOT NULL,
	Price decimal(20, 2) NULL,
	Description varchar(max) NOT NULL,
	StatusId int NOT NULL,
	Quality int NOT NULL,
	ExpiredDate datetime NULL,
	CategoryId int NOT NULL FOREIGN KEY REFERENCES dbo.Category(CategoryId),
	Timestamp timestamp not null
)

CREATE TABLE dbo.Comment(
	CommentId int IDENTITY(1,1) NOT NULL primary key,
	ProductId int NOT NULL FOREIGN KEY REFERENCES dbo.Product(ProductId),
	UserId int NOT NULL FOREIGN KEY REFERENCES dbo.[User](UserId),
	ParentCommentId int NULL,
	Content varchar(max) NOT NULL,
	Timestamp timestamp not null
)

CREATE TABLE dbo.[Order](
	OrderId int IDENTITY(1,1) NOT NULL PRIMARY KEY,
	UserId int NOT NULL FOREIGN KEY REFERENCES dbo.[User](UserId),
	StatusId int NOT NULL,
	Note varchar(max) NULL,
	Address varchar(100) NOT NULL,
	CreatedDate datetime NOT NULL,
	EndDate datetime NULL,
	Phone varchar(20) NOT NULL,
	Timestamp timestamp not null
)

CREATE TABLE dbo.SystemImage(
	ImageId int IDENTITY(1,1) NOT NULL PRIMARY KEY,
	Name varchar(30) NOT NULL,
	Url varchar(max) NOT NULL,
	Timestamp timestamp not null
)

CREATE TABLE dbo.ProductImage(
	ProductImageId int IDENTITY(1,1) NOT NULL PRIMARY KEY,
	ProductId int NOT NULL FOREIGN KEY REFERENCES dbo.Product(ProductId),
	ImageId int NOT NULL FOREIGN KEY REFERENCES dbo.SystemImage(ImageId),
	IsMain bit NULL,
	Timestamp timestamp not null
)

CREATE TABLE dbo.SystemLookupType(
	SystemLookupTypeId int NOT NULL PRIMARY KEY,
	Description varchar(30) NOT NULL,
	Timestamp timestamp not null
)

CREATE TABLE dbo.SystemLookup(
	SystemLookupId int NOT NULL PRIMARY KEY,
	LookupTypeId int NOT NULL FOREIGN KEY REFERENCES dbo.SystemLookupType(SystemLookupTypeId),
	Description varchar(30) NOT NULL,
	LookupReference int NOT NULL,
	Timestamp timestamp not null
)

CREATE TABLE dbo.[Admin](
	AdminId int IDENTITY(1,1) NOT NULL PRIMARY KEY,
	Username varchar(30) NOT NULL,
	Password varchar(100) NOT NULL,
	RoleId int NOT NULL,
	Timestamp timestamp not null
)

CREATE TABLE SystemFeature(
	FeatureId int NOT NULL PRIMARY KEY,
	Description varchar(30) NOT NULL,
	Timestamp timestamp not null
)

CREATE TABLE SystemSecurity(
	SystemSecurityId int IDENTITY(1,1) NOT NULL PRIMARY KEY,
	RoleId int NOT NULL,
	Level int NOT NULL,
	FeatureId int NOT NULL FOREIGN KEY REFERENCES dbo.SystemFeature(FeatureId),
	Timestamp timestamp not null
)

CREATE TABLE OrderProduct(
	OrderProductId int IDENTITY(1,1) NOT NULL PRIMARY KEY,
	ProductId int NOT NULL FOREIGN KEY REFERENCES dbo.Product(ProductId),
	OrderId int NOT NULL FOREIGN KEY REFERENCES dbo.[Order](OrderId),
	Quality int NOT NULL,
	PricePerEach decimal NOT NULL,
	Timestamp timestamp not null
)