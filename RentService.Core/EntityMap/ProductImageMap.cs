﻿using System.ComponentModel.DataAnnotations.Schema;
using RentService.Core.Entity;
using RentService.Core.EntityMap.BaseMap;

namespace RentService.Core.EntityMap
{
    public class ProductImageMap : BaseEntityMap<ProductImage>
    {
        public ProductImageMap()
        {
            this.ToTable("ProductImage");
            this.Property(x => x.Id)
                .HasColumnName("ProductImageId")
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            this.Property(x => x.ImageId).IsRequired();
            this.Property(x => x.IsMain).IsRequired();
            this.Property(x => x.ProductId).IsRequired();
        }
    }
}
