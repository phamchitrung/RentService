﻿using System.ComponentModel.DataAnnotations.Schema;
using RentService.Core.Entity;
using RentService.Core.EntityMap.BaseMap;

namespace RentService.Core.EntityMap
{
    public class SystemImageMap : BaseEntityMap<SystemImage>
    {
        public SystemImageMap()
        {
            this.ToTable("SystemImage");
            this.Property(x => x.Id)
                .HasColumnName("ImageId")
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            this.Property(x => x.Url).IsRequired();
            this.Property(x => x.Name).IsRequired();
        }
    }
}
