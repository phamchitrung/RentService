﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using RentService.Core.Entity.BaseEntity;

namespace RentService.Core.EntityMap.BaseMap
{
    public abstract class BaseEntityMap<TEntity> : EntityTypeConfiguration<TEntity> where TEntity : BaseEntity
    {
        protected BaseEntityMap()
        {
            this.HasKey<int>(s => s.Id);
            this.Property(x => x.Timestamp).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed).IsRowVersion();
        }
    }
}
