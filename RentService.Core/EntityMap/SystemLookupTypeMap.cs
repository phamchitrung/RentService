﻿using RentService.Core.Entity;
using RentService.Core.EntityMap.BaseMap;

namespace RentService.Core.EntityMap
{
    public class SystemLookupTypeMap : BaseEntityMap<SystemLookupType>
    {
        public SystemLookupTypeMap()
        {
            this.ToTable("SystemLookupType");
            this.Property(x => x.Id)
                .HasColumnName("SystemLookupTypeId");
            this.Property(x => x.Description).IsRequired();
        }
    }
}
