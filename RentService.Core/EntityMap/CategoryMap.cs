﻿using System.ComponentModel.DataAnnotations.Schema;
using RentService.Core.Entity;
using RentService.Core.EntityMap.BaseMap;

namespace RentService.Core.EntityMap
{
    public class CategoryMap : BaseEntityMap<Category>
    {
        public CategoryMap()
        {
            this.ToTable("Category");
            this.Property(x => x.Id)
                .HasColumnName("CategoryId")
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            this.Property(x => x.CategoryName).HasColumnName("Name").IsRequired();
            this.Property(x => x.MasterCategoryId).IsOptional();
        }
    }
}
