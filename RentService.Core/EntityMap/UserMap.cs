﻿using System.ComponentModel.DataAnnotations.Schema;
using RentService.Core.Entity;
using RentService.Core.EntityMap.BaseMap;

namespace RentService.Core.EntityMap
{
    public class UserMap : BaseEntityMap<User>
    {
        public UserMap()
        {
            this.ToTable("User");
            this.Property(x => x.Id)
                .HasColumnName("UserId")
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            this.Property(x => x.Address).IsOptional();
            this.Property(x => x.Email).IsRequired();
            this.Property(x => x.Fullname).IsRequired();
            this.Property(x => x.Password).IsRequired();
            this.Property(x => x.Phone).IsOptional();
        }
    }
}
