﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RentService.Core.Entity;
using RentService.Core.EntityMap.BaseMap;

namespace RentService.Core.EntityMap
{
    public class SystemSecurityMap : BaseEntityMap<SystemSecurity>
    {
        public SystemSecurityMap()
        {
            this.ToTable("SystemSecurity");
            this.Property(x => x.Id)
                .HasColumnName("SystemSecurityId")
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            this.Property(x => x.RoleId).IsRequired();
            this.Property(x => x.FeatureId).IsRequired();
            this.Property(x => x.RoleId).IsRequired();
        }
    }
}
