﻿using System.ComponentModel.DataAnnotations.Schema;
using RentService.Core.Entity;
using RentService.Core.EntityMap.BaseMap;

namespace RentService.Core.EntityMap
{
    public class AdminMap : BaseEntityMap<Admin>
    {
        public AdminMap()
        {
            this.ToTable("Admin");
            this.Property(x => x.Id)
                .HasColumnName("AdminId")
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            this.Property(x => x.Username).IsRequired();
            this.Property(x => x.Password).IsRequired();
            this.Property(x => x.RoleId).IsRequired();
        }
    }
}
