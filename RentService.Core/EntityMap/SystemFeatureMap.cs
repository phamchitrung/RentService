﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RentService.Core.Entity;
using RentService.Core.EntityMap.BaseMap;

namespace RentService.Core.EntityMap
{
    public class SystemFeatureMap : BaseEntityMap<SystemFeature>
    {
        public SystemFeatureMap()
        {
            this.ToTable("SystemFeature");
            this.Property(x => x.Id)
                .HasColumnName("FeatureId");
            this.Property(x => x.Description).IsRequired();
        }
    }
}
