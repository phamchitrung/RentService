﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RentService.Core.Entity;
using RentService.Core.EntityMap.BaseMap;

namespace RentService.Core.EntityMap
{
    public class OrderProductMap : BaseEntityMap<OrderProduct>
    {
        public OrderProductMap()
        {
            this.ToTable("OrderProduct");
            this.Property(x => x.Id)
                .HasColumnName("OrderProductId")
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            this.Property(x => x.OrderId).IsRequired();
            this.Property(x => x.ProductId).IsRequired();
            this.Property(x => x.Quality).IsRequired();
            this.Property(x => x.PricePerEach).IsRequired();
        }
    }
}
