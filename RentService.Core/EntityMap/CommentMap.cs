﻿using System.ComponentModel.DataAnnotations.Schema;
using RentService.Core.Entity;
using RentService.Core.EntityMap.BaseMap;

namespace RentService.Core.EntityMap
{
    public class CommentMap : BaseEntityMap<Comment>
    {
        public CommentMap()
        {
            this.ToTable("Comment");
            this.Property(x => x.Id)
                .HasColumnName("CommentId")
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            this.Property(x => x.Content).IsRequired();
            this.Property(x => x.ParentCommentId).IsOptional();
            this.Property(x => x.ProductId).IsRequired();
            this.Property(x => x.UserId).IsRequired();
        }
    }
}
