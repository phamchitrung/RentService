﻿using System.ComponentModel.DataAnnotations.Schema;
using RentService.Core.Entity;
using RentService.Core.EntityMap.BaseMap;

namespace RentService.Core.EntityMap
{
    public class ProductMap : BaseEntityMap<Product>
    {
        public ProductMap()
        {
            this.ToTable("Product");
            this.Property(x => x.Id)
                .HasColumnName("ProductId")
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            this.Property(x => x.Description).IsRequired();
            this.Property(x => x.ExpiredDate).IsOptional();
            this.Property(x => x.Price).IsRequired();
            this.Property(x => x.ProductName).IsRequired();
            this.Property(x => x.Quality).IsRequired();
            this.Property(x => x.StatusId).IsRequired();
            this.Property(x => x.CategoryId).IsRequired();
        }
    }
}
