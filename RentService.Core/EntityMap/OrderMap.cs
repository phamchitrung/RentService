﻿using System.ComponentModel.DataAnnotations.Schema;
using RentService.Core.Entity;
using RentService.Core.EntityMap.BaseMap;

namespace RentService.Core.EntityMap
{
    public class OrderMap : BaseEntityMap<Order>
    {
        public OrderMap()
        {
            this.ToTable("Order");
            this.Property(x => x.Id)
                .HasColumnName("OrderId")
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            this.Property(x => x.Address).IsRequired();
            this.Property(x => x.CreatedDate).IsRequired();
            this.Property(x => x.EndDate).IsOptional();
            this.Property(x => x.Note).IsOptional();
            this.Property(x => x.Phone).IsRequired();
            this.Property(x => x.StatusId).IsRequired();
            this.Property(x => x.UserId).IsRequired();
        }
    }
}
