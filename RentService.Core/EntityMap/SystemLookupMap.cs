﻿using RentService.Core.Entity;
using RentService.Core.EntityMap.BaseMap;

namespace RentService.Core.EntityMap
{
    public class SystemLookupMap : BaseEntityMap<SystemLookup>
    {
        public SystemLookupMap()
        {
            this.ToTable("SystemLookup");
            this.Property(x => x.Id)
                .HasColumnName("SystemLookupId");
            this.Property(x => x.Description).IsRequired();
            this.Property(x => x.LookupReference).IsRequired();
            this.Property(x => x.SystemLookupTypeId).HasColumnName("LookupTypeId").IsRequired();
        }
    }
}
