﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using RentService.Core.Contract;
using RentService.Core.DAO;
using RentService.Core.Repository;

namespace RentService.Core
{
    public class DataModule : Module
    {
        private string connectionString;
        public DataModule(string connString)
        {
            connectionString = connString;
        }
        protected override void Load(ContainerBuilder builder)
        {
            builder.Register(c => new DefaultContext(connectionString)).As<IDbContext>().InstancePerRequest();
            base.Load(builder);
        }
    }
}
