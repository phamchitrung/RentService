﻿namespace RentService.Core.Entity
{
    public class SystemSecurity : BaseEntity.BaseEntity
    {
        public int RoleId { get; set; }
        public int FeatureId { get; set; }
        public int Level { get; set; }
    }
}
