﻿namespace RentService.Core.Entity.BaseEntity
{
    public class PagingQuery
    {
        public int PageIndex { get; set; }
        public int PageLength { get; set; }

        public PagingQuery(int pageIndex, int pageLength)
        {
            PageIndex = pageIndex;
            PageLength = pageLength;
        }
    }
}
