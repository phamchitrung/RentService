﻿namespace RentService.Core.Entity.BaseEntity
{
    public abstract class BaseEntity
    {
        public int Id { get; set; }
        public byte[] Timestamp { get; set; }
    }
}
