﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RentService.Core.Entity
{
    public class OrderProduct : BaseEntity.BaseEntity
    {
        public int OrderId { get; set; }
        public int ProductId { get; set; }
        public int Quality { get; set; }
        public decimal PricePerEach { get; set; }
    }
}
