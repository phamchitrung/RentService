﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RentService.Core.Entity
{
    public class Category : BaseEntity.BaseEntity
    {
        public string CategoryName { get; set; }
        public int? MasterCategoryId { get; set; }
    }
}
