﻿namespace RentService.Core.Entity
{
    public class Comment : BaseEntity.BaseEntity
    {
        public int ProductId { get; set; }
        public int UserId { get; set; }
        public int? ParentCommentId { get; set; }
        public string Content { get; set; }
    }
}
