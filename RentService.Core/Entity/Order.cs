﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RentService.Core.Entity
{
    public class Order : BaseEntity.BaseEntity
    {
        public int UserId { get; set; }
        public int StatusId { get; set; }
        public string Note { get; set; }
        public string Address { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Phone { get; set; }
    }
}
