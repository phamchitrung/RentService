﻿using System;

namespace RentService.Core.Entity
{
    public class Product : BaseEntity.BaseEntity
    {
        public string ProductName { get; set; }
        public decimal Price { get; set; }
        public string Description { get; set; }
        public int StatusId { get; set; }
        public int Quality { get; set; }
        public int CategoryId { get; set; }
        public DateTime? ExpiredDate { get; set; }
    }
}
