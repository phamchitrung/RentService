﻿using RentService.Core.Entity.BaseEntity;

namespace RentService.Core.Entity
{
    public class SystemLookup : BaseEntity.BaseEntity
    {
        public string Description { get; set; }
        public int SystemLookupTypeId { get; set; }
        public int LookupReference { get; set; }
    }
}
