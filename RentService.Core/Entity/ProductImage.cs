﻿using RentService.Core.Entity.BaseEntity;

namespace RentService.Core.Entity
{
    public class ProductImage : BaseEntity.BaseEntity
    {
        public int ProductId { get; set; }
        public int ImageId { get; set; }
        public bool IsMain { get; set; }
    }
}
