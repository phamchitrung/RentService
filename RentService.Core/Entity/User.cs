﻿namespace RentService.Core.Entity
{
    public class User : BaseEntity.BaseEntity
    {
        public string Fullname { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string Password { get; set; }
    }
}
