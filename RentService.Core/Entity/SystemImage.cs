﻿namespace RentService.Core.Entity
{
    public class SystemImage : BaseEntity.BaseEntity
    {
        public string Name { get; set; }
        public string Url { get; set; }
    }
}
