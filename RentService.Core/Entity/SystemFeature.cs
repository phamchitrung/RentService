﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RentService.Core.Entity
{
    public class SystemFeature : BaseEntity.BaseEntity
    {
        public string Description { get; set; }
    }
}
