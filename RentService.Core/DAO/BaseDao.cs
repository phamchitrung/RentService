﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Common.CommandTrees;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using RentService.Core.Contract;
using RentService.Core.Entity;
using RentService.Core.Entity.BaseEntity;

namespace RentService.Core.DAO
{
    public class BaseDao<TEntity> : IDao<TEntity> where TEntity : BaseEntity
    {
        private readonly IDbContext dbContext;

        public BaseDao(IDbContext context)
        {
            dbContext = context;
        }

        public TEntity GetEntityById(int id)
        {
            return dbContext.Set<TEntity>().Find(id);
        }

        public IList<TEntity> GetEntitiesPaging(PagingQuery query)
        {
            return dbContext.Set<TEntity>().Skip(query.PageIndex * query.PageLength).Take(query.PageLength).ToList();
        }

        public TEntity Save(TEntity entity)
        {
            if (entity == null) return null;
            var dbEntity = dbContext.Set<TEntity>().FirstOrDefault(x => x.Id == entity.Id);
            if (dbEntity == null)
            {
                dbContext.Set<TEntity>().Add(entity);
            }
            else
            {
                dbContext.Entry(dbEntity).CurrentValues.SetValues(entity);
            }
            dbContext.SaveChanges();
            return dbContext.Entry(entity).Entity;
        }

        public bool Delete(int id)
        {
            var entity = GetEntityById(id);
            if (entity == null) return false;
            dbContext.Entry(entity).State = EntityState.Deleted;
            dbContext.SaveChanges();
            return true;
        }

        public IList<TEntity> List()
        {
            return dbContext.Set<TEntity>().ToList();
        }

        public IQueryable<TEntity> Query(Expression<Func<TEntity, bool>> where, PagingQuery pageQuery = null)
        {
            if (pageQuery != null)
            {
                return dbContext.Set<TEntity>().Where(where).Skip((pageQuery.PageIndex - 1) * pageQuery.PageLength).Take(pageQuery.PageLength);
            }
            return dbContext.Set<TEntity>().Where(where);
        }
        public void SaveAll(IList<TEntity> entities)
        {
            if (entities == null) return;
            foreach (var entity in entities)
            {
                var dbEntity = dbContext.Set<TEntity>().FirstOrDefault(x => x.Id == entity.Id);
                if (dbEntity == null)
                {
                    dbContext.Set<TEntity>().Add(entity);
                }
                else
                {
                    dbContext.Entry(dbEntity).CurrentValues.SetValues(entity);
                }
            }
            dbContext.SaveChanges();
        }
    }
}
