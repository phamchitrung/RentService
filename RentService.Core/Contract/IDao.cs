﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using RentService.Core.Entity;
using RentService.Core.Entity.BaseEntity;

namespace RentService.Core.Contract
{
    public interface IDao<TEntity> where TEntity : BaseEntity
    {
        TEntity GetEntityById(int id);
        IList<TEntity> GetEntitiesPaging(PagingQuery query);
        TEntity Save(TEntity entity);
        void SaveAll(IList<TEntity> entities);
        bool Delete(int id);
        IList<TEntity> List();

        IQueryable<TEntity> Query(Expression<Func<TEntity, bool>> where, PagingQuery pageQuery = null);
    }
}
