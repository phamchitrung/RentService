﻿; (function ($) {
    UtilityClass = function () { };

    UtilityClass.prototype = {
        init: UtilityClass,
        resolveUrl: function (url) {
            return $('#resolve-url').val() + url;
        },
        extend: function (protobj, skipBaseConstructor) {
            protobj = protobj || {};
            var subClass = null;
            var baseConstructor = this;
            if (typeof (baseConstructor) != "function") {
                baseConstructor = this.init;
            }

            if (protobj.init) {
                subClass = function () {
                    if (!skipBaseConstructor) {
                        baseConstructor.apply(this, arguments);
                    }
                    protobj.init.apply(this, arguments);
                };
            } else {
                subClass = function () {
                    if (!skipBaseConstructor) {
                        baseConstructor.apply(this, arguments);
                    }
                };
                protobj.init = baseConstructor;
            }
            subClass.prototype = subClass.prototype || {};
            $.extend(true, subClass.prototype, this.prototype, protobj);
            subClass.extend = this.extend;
            return subClass;
        },
        showPopup: function(title, message) {
            $("#myPopupLabel").text(title);
            $("#popup-content").text(message);
            $("#messagePopup").modal('show');
        }
    }

    Helpers = new UtilityClass();

    //Core functions
    Helpers.AjaxCore = Helpers.extend({
        JSON: 'json',
        HTML: 'html',
        POST: 'POST',
        GET: 'GET',
        SLASH: '/',
        AND: '&',
        QUESTION_MARK: '?',

        //called when an ajax request is completed
        ajaxComplete: function (uid, mask) {
            if (mask) {
                if (mask.UIDBlock === undefined) {
                    $.unblockUI();
                    mask = null;
                } else {
                    $('.' + mask.UIDBlock).unblock();
                    mask = null;
                }
            }
            //$('header').css('z-index', 1000);

        },

        buildUrl: function (controller, action) {
            var rootUrl = this.getRootUrl();
            var url = rootUrl + controller + this.SLASH + action;
            return url;//.replace("//", "/");
        },

        getRootUrl: function () {
            var rootUrl = window.location.origin;
            if (/\/.+/.test(rootUrl)) {
                rootUrl = rootUrl + this.SLASH;
            }
            return rootUrl;
        },

        //send ajax request
        ajax: function (options) {
            var url = options.url;

            if (!url) {
                url = this.buildUrl(options.controller, options.action);
            }
            
            return $.ajax({
                url: url,
                data: options.data,
                dataType: options.dataType,
                type: options.type,
                cache: options.cache,
                async: true,
                context: this,
                success: function (result) {
                    try {
                        options.success.call(this, result);
                    } catch (error) {
                        console.log(error);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if (typeof options.error === 'function') {
                        options.error.call(this, jqXHR, textStatus);
                    }
                },

                complete: function (jqXHR, textStatus) {
                    if (typeof options.complete === 'function') {
                        options.complete.call(this, jqXHR, textStatus);
                    }
                },

            });

            //return false;
        },

        //send ajax request with data in JSON format and GET verb
        getJson: function (options) {
            var defaultOptions = {
                dataType: this.JSON,
                type: this.GET
            };
            var ajaxOptions = $.extend({}, defaultOptions, options);
            this.ajax(ajaxOptions);
        },

        //send ajax request with data in JSON format and POST verb
        postJson: function (options) {
            var defaultOptions = {
                dataType: this.JSON,
                type: this.POST
            };
            var ajaxOptions = $.extend({}, defaultOptions, options);
            return this.ajax(ajaxOptions);
        },

        //send ajax request with data in HTML format and GET verb
        getHtml: function (options) {
            var defaultOptions = {
                dataType: this.HTML,
                type: this.GET
            };
            var ajaxOptions = $.extend({}, defaultOptions, options);
            return this.ajax(ajaxOptions);
        },

        //send ajax request with data in HTML format and POST verb
        postHtml: function (options) {
            var defaultOptions = {
                dataType: this.HTML,
                type: this.POST
            };
            var ajaxOptions = $.extend({}, defaultOptions, options);
            return this.ajax(ajaxOptions);
        }
    });
    Helpers.ajaxHelper = new Helpers.AjaxCore();
})(jQuery);