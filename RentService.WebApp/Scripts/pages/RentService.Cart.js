﻿;
(function ($, RentService, undefined) {
    RentService = RentService || {};
    RentService.Cart = {
        init: function () {
            var that = this;
            that.registerEvents();
        },

        registerEvents: function () {
            $(document).on('click', "#updateCartBtn", function (e) {
                e.preventDefault();
                RentService.MasterPage.getContent("Cart", "EditCart", $('#cart-form').serialize(), $("#main_content"));
            });
            $(document).on('click', ".removeRow", function (e) {
                e.preventDefault();
                var rowIndex = $(".removeRow").index($(this));
                $(".cartRow").eq(rowIndex).remove();
                RentService.MasterPage.getContent("Cart", "EditCart", $('#cart-form').serialize(), $("#main_content"));
            });
        }
    };
})(jQuery, window.RentService = window.RentService || {}, window.Helpers);
$(document).ready(function () {
    RentService.Cart.init();
});