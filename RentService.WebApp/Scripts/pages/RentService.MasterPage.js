﻿;
(function ($, RentService, undefined) {
    RentService = RentService || {};
    RentService.MasterPage = {
        init: function () {
            this.registerEvents();
            this.showInitMessage();
        },

        registerEvents: function () {
            $(document).on("keydown", "input.number", function (e) {
                // Allow: backspace, delete, tab, escape, enter and .
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                    // Allow: Ctrl+A, Command+A
                    (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                    // Allow: home, end, left, right, down, up
                    (e.keyCode >= 35 && e.keyCode <= 40)) {
                    // let it happen, don't do anything
                    return;
                }
                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            });
            $(document).on("blur", "input.number", function (e) {
                var min = $(this).attr("min"),
                    max = $(this).attr("max");
                if (typeof (min) !== "undefined" && $(this).val() < min) {
                    $(this).val(min);
                }
                if (typeof (max) !== "undefined" && $(this).val() > max) {
                    $(this).val(max);
                }
            });
        },
        //openProductDetail: function (productId, e) {
        //    e.preventDefault();
        //    var that = RentService.MasterPage;
        //    that.getContent("Product", "Index", { id: productId }, $("#main_content"));
        //},
        getContent: function (controller, action, data, $content, callback, error) {
            window.Helpers.ajaxHelper.postHtml({
                controller: controller,
                action: action,
                data: data,
                success: function (result) {
                    $content.html(result);
                    callback.call();
                },
                handleErrorDefault: true,
                async: true,
                showMask: true
            });
        },
        addToCart: function (productId, e) {
            e.preventDefault();
            console.log(productId);
            window.Helpers.ajaxHelper.postJson({
                controller: "Cart",
                action: "AddProduct",
                data: {
                    productId: productId
                },
                success: function (result) {
                    window.Helpers.showPopup("Information", "Product has been added to your cart!");
                },
                handleErrorDefault: true,
                async: true,
                showMask: true
            });
        },
        showInitMessage: function () {
            if ($("#hasMessage").val() != "") {
                $("#messagePopup").modal('show');
            }
        }
    };
})(jQuery, window.RentService = window.RentService || {}, window.Helpers);
$(document).ready(function () {
    RentService.MasterPage.init();
});