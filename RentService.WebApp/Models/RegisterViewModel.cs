﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RentService.WebApp.Models
{
    public class RegisterViewModel
    {
        [Required]
        public string Fullname { get; set; }
        [Required]
        [MinLength(6, ErrorMessage = "Password must have at least 6 characters")]
        public string Password { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public string Address { get; set; }
        [Required]
        public string Phone { get; set; }
    }
}