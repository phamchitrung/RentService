﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RentService.Service.Model;

namespace RentService.WebApp.Models
{
    public class IndexPageViewModel
    {
        public List<CartItemModel> LatestProducts { get; set; }
        public List<CartItemModel> RecommendedProducts { get; set; }
    }
}