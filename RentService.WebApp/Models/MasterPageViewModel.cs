﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RentService.Service.Model;
using RentService.Service.Model.Base;

namespace RentService.WebApp.Models
{
    public class MasterPageViewModel
    {
        public List<CartItemModel> CartItems { get; set; }
        public List<MasterCategoryModel> MasterCategories { get; set; }
        public string SearchText { get; set; }
    }
}