﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RentService.Service.Model;

namespace RentService.WebApp.Models
{
    public class SearchProductViewModel
    {
        public string Title { get; set; }
        public List<CartItemModel> Products { get; set; }
    }
}