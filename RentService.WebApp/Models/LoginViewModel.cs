﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RentService.WebApp.Models
{
    public class LoginViewModel
    {
        [Required]
        public string Password { get; set; }
        [Required]
        public string Email { get; set; }
    }
}