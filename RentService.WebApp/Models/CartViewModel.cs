﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RentService.Service.Model;

namespace RentService.WebApp.Models
{
    public class CartViewModel
    {
        public List<CartItemModel> CartItems { get; set; }
        public OrderInformationModel OrderInformation { get; set; }
    }
}