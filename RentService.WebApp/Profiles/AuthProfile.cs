﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using RentService.Service.Model;
using RentService.WebApp.Models;

namespace RentService.WebApp.Profiles
{
    public class AuthProfile : Profile
    {
        public AuthProfile()
        {
            CreateMap<LoginViewModel, UserDetailModel>();
            CreateMap<RegisterViewModel, UserDetailModel>();

            CreateMap<UserDetailModel, UserDetailViewModel>();
            CreateMap<UserDetailViewModel, UserDetailModel>();
        }
    }
}