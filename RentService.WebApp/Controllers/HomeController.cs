﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using RentService.Service.Contract;
using RentService.Service.Model;
using RentService.Service.Model.Base;
using RentService.WebApp.Models;

namespace RentService.WebApp.Controllers
{
    public class HomeController : Controller
    {
        private IMyAuthenticationService myAuthenticationService;
        private IProductService productService;
        public HomeController(IMyAuthenticationService myAuthenticationService, IProductService productService)
        {
            this.myAuthenticationService = myAuthenticationService;
            this.productService = productService;
        }

        public ActionResult Index()
        {
            //this.myAuthenticationService.Initial();
            var viewModel = new IndexPageViewModel()
            {
                LatestProducts = productService.GetLastestProducts(),
                RecommendedProducts = productService.GetRecommendedProducts()
            };
            ViewBag.Message = TempData["Message"];
            return View(viewModel);
        }

        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ActionName("Login")]
        public ActionResult UserLogin(LoginViewModel userDetail)
        {
            if (!ModelState.IsValid) return View("Login", userDetail);
            var validations =
                myAuthenticationService.Login(Mapper.Map<UserDetailModel>(userDetail));
            if (validations.Count <= 0)
            {
                Session["Username"] = userDetail.Email;
                return RedirectToAction("Index", "Home");
            } 
            validations.ForEach(x => ModelState.AddModelError(x.Field, x.ValidateMessage));
            return View(userDetail);
        }

        [HttpGet]
        public ActionResult Detail()
        {
            if (Session["Username"] == null) return RedirectToAction("Index");
            var email = Session["Username"].ToString();
            var userDetail = myAuthenticationService.GetUserDetail(email);
            var userViewModel = Mapper.Map<UserDetailViewModel>(userDetail);
            return View(userViewModel);
        }

        [HttpPost]
        [ActionName("Detail")]
        public ActionResult UpdateUser(UserDetailViewModel userDetail)
        {
            if (!ModelState.IsValid) return View(userDetail);
            var userModel = Mapper.Map<UserDetailModel>(userDetail);
            myAuthenticationService.UpdateUser(userModel);
            TempData["Message"] = "Saved!";
            return RedirectToAction("Index");
        }

        public ActionResult Logout()
        {
            Session["Username"] = null;
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Register()
        {
            var viewModel = new RegisterViewModel();
            return View(viewModel);
        }

        [HttpPost]
        [ActionName("Register")]
        public ActionResult UserRegister(RegisterViewModel userDetail)
        {
            if (!ModelState.IsValid) return View("Register", userDetail);
            var userModel = Mapper.Map<UserDetailModel>(userDetail);
            var validations = myAuthenticationService.ValidateUser(userModel);
            if (validations.Count <= 0)
            {
                myAuthenticationService.SaveUser(userModel);
                TempData["Message"] = "Register successfully!";
                return RedirectToAction("Index", "Home");
            }
            validations.ForEach(x => ModelState.AddModelError(x.Field, x.ValidateMessage));
            return View(userDetail);
        }
    }
}