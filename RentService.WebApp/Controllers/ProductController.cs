﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RentService.Service.Contract;
using RentService.Service.Model.Base;
using RentService.WebApp.Models;

namespace RentService.WebApp.Controllers
{
    public class ProductController : Controller
    {
        private IProductService productService;
        private const int PAGE_SIZE = 9;
        public ProductController(IProductService productService)
        {
            this.productService = productService;
        }

        public ActionResult Index(int id)
        {
            var viewModel = new ProductDetailViewModel()
            {
                ProductDetail = productService.GetProductDetail(id)
            };
            return View(viewModel);
        }

        public ActionResult SearchProduct(int? categoryId, string textSearch, int pageIndex = 1)
        {
            var viewModel = new SearchProductViewModel()
            {
                Products = productService.SearchProduct(categoryId, textSearch, new PagingQueryModel(pageIndex, PAGE_SIZE)),
                Title = "Result"
            };
            return View(viewModel);
        }
    }
}