﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RentService.Service.Contract;
using RentService.Service.Model;
using RentService.WebApp.Helper;
using RentService.WebApp.Models;

namespace RentService.WebApp.Controllers
{
    public class CartController : Controller
    {
        private IProductService productService;
        private IOrderService orderService;
        private IMyAuthenticationService myAuthenticationService;

        public CartController(IProductService productService, IOrderService orderService, IMyAuthenticationService myAuthenticationService)
        {
            this.productService = productService;
            this.orderService = orderService;
            this.myAuthenticationService = myAuthenticationService;
        }

        [HttpGet]
        public ActionResult Index()
        {
            ViewBag.Message = TempData["Message"];
            var viewModel = new CartViewModel()
            {
                CartItems =
                    Session["Cart"] == null
                        ? new List<CartItemModel>()
                        : productService.GetProducts((Dictionary<int, int>) Session["Cart"])
            };
            return View(viewModel);
        }

        [HttpGet]
        public ActionResult Information()
        {
            var viewModel = new CartViewModel()
            {
                OrderInformation = new OrderInformationModel()
            };
            return View(viewModel);
        }

        public ActionResult EditCart(CartViewModel model)
        {
            model = SetCartSession(model);
            return View("Index", model);
        }

        private CartViewModel SetCartSession(CartViewModel model)
        {
            var cartItems = model.CartItems ?? new List<CartItemModel>();
            var cart = cartItems.Where(cartItem => cartItem != null && cartItem.Quality != 0).ToDictionary(cartItem => cartItem.Id, cartItem => cartItem.Quality);
            Session["Cart"] = cart;
            model.CartItems =
                Session["Cart"] == null
                    ? new List<CartItemModel>()
                    : productService.GetProducts((Dictionary<int, int>) Session["Cart"]);
            return model;
        }

        [HttpPost]
        [ActionName("Index")]
        public ActionResult SetCart(CartViewModel model)
        {
            if (!ModelState.IsValid) return View(model);
            model = SetCartSession(model);
            if (Session["Username"] != null) return RedirectToAction("Information");
            ViewBag.Message = "You need to login to checkout!";
            return View(model);
        }

        private int GetUserId()
        {
            return Session["Username"] == null ? 0 : myAuthenticationService.GetUserId(Session["Username"].ToString());
        }

        [HttpPost]
        [ActionName("Information")]
        public ActionResult Checkout(OrderInformationModel OrderInformation)
        {
            if (!ModelState.IsValid)
            {
                return View(new CartViewModel() {OrderInformation = OrderInformation});
            }
            var cartProducts = productService.GetProducts((Dictionary<int, int>) Session["Cart"]);
            orderService.SaveOrder(OrderInformation, cartProducts, GetUserId());
            Session["Cart"] = null;
            TempData["Message"] = "Your order has been sent to server!";
            return RedirectToAction("Index", "Home");
        }

        public JsonResult AddProduct(int productId)
        {
            if (Session["Cart"] == null)
            {
                var cart = new Dictionary<int, int> { { productId, 1 } };
                Session["Cart"] = cart;
            }
            else
            {
                var cart = (Dictionary<int, int>)Session["Cart"];
                if (cart.ContainsKey(productId))
                {
                    cart[productId]++;
                }
                else
                {
                    cart.Add(productId, 1);
                }
            }
            return Json(true, JsonRequestBehavior.AllowGet);
        }
    }
}