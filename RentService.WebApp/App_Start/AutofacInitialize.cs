﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Configuration;
using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using AutoMapper;
using RentService.Service;
using RentService.Service.Contract;
using RentService.Service.Model;
using RentService.Service.Profiles;
using RentService.Service.Service;
using RentService.WebApp.Models;
using RentService.WebApp.Profiles;

namespace RentService.WebApp
{
    public class AutofacInitialize
    {
        public static void InitializeIoc()
        {
            var builder = new ContainerBuilder();
            builder.RegisterControllers(typeof(MvcApplication).Assembly);
            var connectionString = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            RegisterService(builder);
            builder.RegisterModule(new ServiceModule(connectionString));

            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
            InitMapper();
        }

        public static void RegisterService(ContainerBuilder builder)
        {
            builder.RegisterType<MyAuthenticationService>().As<IMyAuthenticationService>().InstancePerRequest();
            builder.RegisterType<MasterService>().As<IMasterService>().InstancePerRequest();
            builder.RegisterType<ProductService>().As<IProductService>().InstancePerRequest();
            builder.RegisterType<OrderService>().As<IOrderService>().InstancePerRequest();
        }

        public static void InitMapper()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile(new UserProfile());
                cfg.AddProfile(new AuthProfile());
            });
        }

        //private static IEnumerable<Type> GetProfiles()
        //{
        //    var types =  typeof(MvcApplication).Assembly.GetTypes()
        //        .Where(type => !type.IsAbstract && typeof(Profile).IsAssignableFrom(type));
        //    foreach (var type in types)
        //    {
                
        //    }
        //}
    }
}